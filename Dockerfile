FROM ubuntu:latest

RUN apt update
RUN apt install python3 -y

WORKDIR /usr/app/src

COPY my_fist_script.py /usr/app/src

CMD ["python", "./my_fist_script.py"]

